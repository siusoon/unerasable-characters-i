## Unerasable Characters I

Refer to [here](https://siusoon.net/projects/unerasablecharacters-i) for the project description.

**Acknowledgement of the project:**
- Dr. Fu King-wa, Weiboscope, The University of Hong Kong
- Olle Essvik and Joel Nordqvist, [Rojal](https://www.rojal.se/)
- Greg Surma, [Text Predictor](https://github.com/gsurma/text_predictor) 
- Miriam Kelly & Shelly McSpedden
- Australian Centre for Contemporary Art

**Colophon:**

訂裝工具 / Book Binding Tool: [Rojal](https://www.rojal.se/)

開源中文字體 / Open Chinese font: [源流明體](https://github.com/ButTaiwan/genryu-font)

編程語言 / Programming language: HTML + CSS + Paged.js

更多資訊 / For more information: https://siusoon.net/

© CC-BY-SA 2022

### Print your own book: 

0. You can see how it looks [here](https://siusoon.gitlab.io/unerasable-characters-i/htmltoprint/output.html)
1. Fork the repository 
2. Modify Colophon, Style with CSS, and content in the HTML 
3. Use the browser chrome to open the file output.html
4. Press Print -> Choose PDF -> Save to your local drive 

### Existing Print setting 

- Paper: white 90gsm 
- Actual width-left margin (for the content): 8.8 cm
- Print setup: Actual, Landscape
- Total page: 2652 pp

### Existing binding tools

- 2x book binding wood (24.6x2x0.8 cm), hole's diameter: 0.5 cm
- 2x wing nuts (M5=5mm)
- 2x flat washers (M5=5mm)
- 2x long headless screws (35 cm)
- 1x A4 textile cloth for the book cover 

or you can buy it [here](https://rojal.se/index2.htm) from the publisher [Rojal](https://rojal.se/)
